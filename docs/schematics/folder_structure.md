## Folder Structure schematic  <!-- {docsify-ignore} -->

### What is the porpuse of this schematic?

This schematic is very important when you start a new project and you want to create an especific folder arquitecture.
Sometimes you spend a lot of time creating each folder, module and adding short-imports.

For this reason, When you use this schematic, you will have 2 options:

1. Select an default folder structure..
2. Select your custom folder structure.

### Installation and how to test it locally?

[CONTRIBUTION GUIDELINE](CONTRIBUTION_GUIDELINES.md)

### Options when you execute the schematic

1. **Select an Scaffold structure:** select default o custom scaffold.
2. **Write the relative project url:** if you have many projects in the same workspace you need to write for example: projects/secondary-application.
3. **Write the url of your files structure file json:** By default, the system will search in the root of the workspace the file _customStructure.json_.

### Structure of JSON File

#### Interface Name: FolderStructure

| Attribute    | Type                     | Optional |
| ------------ | ------------------------ | -------- |
| name         | string                   | false    |
| hasModule    | boolean                  | true     |
| hasShortPath | boolean                  | true     |
| hasRouting   | boolean                  | true     |
| children     | Array of FolderStructure | true     |
