## Auth Module  <!-- {docsify-ignore} -->

### What is the porpuse of this schematic?

The porpuse of this schematic is to generate the auth module with a complete functiontionality when Lion Login option is selected. For other options will generate generic files with the correct structure.

For this reason, When you use this schematic, you will have 2 options:

1. Select Lion Login.
2. Select Custom Login.

### Installation

```sh
npm install
npm install -g @angular-devkit/schematics-cli
```

### How to test it locally

1. Open visual studio code
2. Open workspace from file
3. Open a new terminal select the initial-application or
   ```
   cd pgd-schematics
   ```
4. Execute
   ```
   npm run build-watch
   ```
5. Open a new terminal select the initial-application or
   ```
   cd initial-application
   ```
6. Execute
   ```
   schematics ../pgd-schematics/src/collection.json:new-auth-module --routing  --debug=false --force
   ```

### Options when you execute the schematic

1. **Select an Auth Type:** select LL o custom login.
2. **Write the path of your module auth:** path in which the auth module will be generated, default _src/app/core_.
3. **What name would you like to use for the login component?:** Name of the login component, default _login_.
4. **Type the URL of the login API:** URL of the login API, default _/api/_.
5. **Type the default route of the project:** URL of the application where the user will be redirected when the login succeed, for example: _/home_. The default value is _/_.
6. **Write the relative project url:** if you have many projects in the same workspace you need to write for example: projects/secondary-application.

### Files generated after schematic execution

Only the Lion Login option is fully functional if another option is selected, the files will be generated as empty.

**If Lion Login option was selected:**

- authentication
  - login
    - login.component.ts
    - login.component.scss
    - login.component.spec.ts
    - login.component.ts
  - auth-guard.service.ts
  - auth-http-client.ts
  - auth-user.interface.ts
  - auth.interceptor.ts
  - auth.module.ts
  - auth.service.ts

If the flag --routing was set, the AppRouting file will be generated with the correct implementation of the guard. If you have an existing app routing file, you have to edit adding the routers for login and the guard service. You also have to add the providers for the interceptor in the app module.

**If Custom Login option was selected:**

- authentication
  - login
    - login.component.ts
    - login.component.scss
    - login.component.spec.ts
    - login.component.ts
  - auth-user.interface.ts
  - auth.module.ts
  - auth.service.ts

If the flag --routing was set, the AppRouting file will be updated with the appRouting import.
