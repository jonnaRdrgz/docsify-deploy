# Contribution Guidelines

Contributors need to adhere to the following guidelines. For questions, please reach out to OneWeb technology leads or add an issue in this repository.

Further details on how to contribute can be found in the [development workflow](DEVELOPMENT_WORKFLOW.md)

## Getting Started

- Fork the repository.
- Clone the fork locally.
- Run `npm install`.
- Run `npm install -g @angular-devkit/schematics-cli`.
- Run `npm install -g verdaccio`.

### Synchronize

When preparing to contribute an update, synchronize the local copy of the main branch (`origin/main`).

### Branch Naming Convention

The branch naming convention is important for a few reasons:

- Identifies the type of changes that are in the branch.
- Organizes the branch locally by convention and type.
- Labels the branch to identify who has contributed the work.

A branch label contains these parts:

- [TYPE], such as a feature, bug, hotfix, or task.
- [ISSUE NUMBER], that can be either a github issue or Jira ticket number.
- [SHORT TITLE], that can help briefly describe the work.
- [INITIALS], that denotes the author of the work.

The parts are concatenated like this:

```
[TYPE]/[ISSUE NUMBER]_[SHORT TITLE]_[INITIALS]
```

Example:

```bash
$ git checkout -b feature/PGDSP-2212_Authentication_DR
```

## Create the new package version.

You need to create a new package.json version.

- Run `npm version v.v.v`, check the currently version and go to the next one.
- Run `npm run build:lib`;
- Run `cd dist`;
- Run `verdaccio` in **another console**;
- Run `npm publish --registry http://localhost:4873`;

**Note**
It's a good idea if you have 4 terminals:

1. In the pgd-schematics projects one with verdacccio running.
2. In the pgd-schematics projects other to update and build the solution.
3. And the another in dist folder of the pgd-schematics projects to publish the package.
4. In your angular project folder.
   **If you already installed and publish a version of the package, please close and open a new terminal with the dist folder.**

## How to test the new version.

- go to your project
- run `ng add @pgd/schematics --registry http://localhost:4873`;

if you already added it

- run `ng add @pgd/schematics@[version] --registry http://localhost:4873`;

When everything is install you can test you schematics

- run `ng g @pgd/schematics:[name-of-the-schematic]`;

## Testing

## Documenting Source Code

## Development Workflow

Read the [development workflow](DEVELOPMENT_WORKFLOW.md) to further understand the process on how to contribute.
