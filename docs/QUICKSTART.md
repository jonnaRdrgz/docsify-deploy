# Quick start

To start using the angular schematics package in your project, you need to follow the steps below:

## Authenticate
We will use the personal access token to authenticate in the registry. If you don't have one, please refer to the following link to create one [create personal access token](https://gitlab.prodigious.com/help/user/profile/personal_access_tokens.md).

Next is configure your `.npmrc` file. Your file should look like this:

```
strict-ssl=false
@pgd:registry=https://gitlab.prodigious.com/api/v4/projects/458/packages/npm/
//gitlab.prodigious.com/api/v4/packages/npm/:_authToken=<your_token>
//gitlab.prodigious.com/api/v4/projects/458/packages/npm/:_authToken=<your_token>
```
- `<your_project_id>` is your personal access token or deploy token.

Please refer to the next link to see the entire documentation and authentication methods: [Authenticate to the package registry](https://gitlab.prodigious.com/help/user/packages/npm_registry/index#authenticate-to-the-package-registry).

## Install
You should now be able to install the package in your project just run the following step:
```bash
npm install @pgd/schematics

```

## Use the schematics
Now you can start using the schematics with the following command
```bash
ng g @pgd/schematics:[name-of-the-schematic]

```

