# Schematics Documentation

- [Schematics Documentation](#schematics-documentation)
  - [Folder Structure schematic](#folder-structure-schematic)
    - [What is the porpuse of this schematic?](#what-is-the-porpuse-of-this-schematic)
    - [Installation and how to test it locally?](#installation-and-how-to-test-it-locally)
    - [Options when you execute the schematic](#options-when-you-execute-the-schematic)
    - [Structure of JSON File](#structure-of-json-file)
      - [Interface Name: FolderStructure](#interface-name-folderstructure)
  - [Auth Module](#auth-module)
    - [What is the porpuse of this schematic?](#what-is-the-porpuse-of-this-schematic-1)
    - [Installation](#installation)
    - [How to test it locally](#how-to-test-it-locally)
    - [Options when you execute the schematic](#options-when-you-execute-the-schematic-1)
    - [Files generated after schematic execution](#files-generated-after-schematic-execution)

## Folder Structure schematic

### What is the porpuse of this schematic?

This schematic is very important when you start a new project and you want to create an especific folder arquitecture.
Sometimes you spend a lot of time creating each folder, module and adding short-imports.

For this reason, When you use this schematic, you will have 2 options:

1. Select an default folder structure..
2. Select your custom folder structure.

### Installation and how to test it locally?

[CONTRIBUTION GUIDELINE](CONTRIBUTION_GUIDELINES.md)

### Options when you execute the schematic

1. **Select an Scaffold structure:** select default o custom scaffold.
2. **Write the relative project url:** if you have many projects in the same workspace you need to write for example: projects/secondary-application.
3. **Write the url of your files structure file json:** By default, the system will search in the root of the workspace the file _customStructure.json_.

### Structure of JSON File

#### Interface Name: FolderStructure

| Attribute    | Type                     | Optional |
| ------------ | ------------------------ | -------- |
| name         | string                   | false    |
| hasModule    | boolean                  | true     |
| hasShortPath | boolean                  | true     |
| hasRouting   | boolean                  | true     |
| children     | Array of FolderStructure | true     |

## Auth Module

### What is the porpuse of this schematic?

The porpuse of this schematic is to generate the auth module with a complete functiontionality when Lion Login option is selected. For other options will generate generic files with the correct structure.

For this reason, When you use this schematic, you will have 2 options:

1. Select Lion Login.
2. Select Custom Login.

### Installation

```sh
npm install
npm install -g @angular-devkit/schematics-cli
```

### How to test it locally

1. Open visual studio code
2. Open workspace from file
3. Open a new terminal select the initial-application or
   ```
   cd pgd-schematics
   ```
4. execute
   ```
   npm run build-watch
   ```
5. Open a new terminal select the initial-application or
   ```
   cd initial-application
   ```
6. execute
   ```
   schematics ../pgd-schematics/src/collection.json:new-auth-module --routing  --debug=false --force
   ```

### Options when you execute the schematic

1. **Select an Auth Type:** select LL o custom login.
2. **Write the path of your module auth:** path in which the auth module will be generated, default _src/app/core_.
3. **What name would you like to use for the login component?:** Name of the login component, default _login_.
4. **Type the URL of the login API:** URL of the login API, default _/api/_.
5. **Type the default route of the project:** URL of the application where the user will be redirected when the login succeed, for example: _/home_. The default value is _/_.
6. **Write the relative project url:** if you have many projects in the same workspace you need to write for example: projects/secondary-application.

### Files generated after schematic execution

Only the Lion Login option is fully functional if another option is selected, the files will be generated as empty.

**If Lion Login option was selected:**

- authentication
  - login
    - login.component.ts
    - login.component.scss
    - login.component.spec.ts
    - login.component.ts
  - auth-guard.service.ts
  - auth-http-client.ts
  - auth-user.interface.ts
  - auth.interceptor.ts
  - auth.module.ts
  - auth.service.ts

If the flag --routing was set, the AppRouting file will be generated with the correct implementation of the guard. If you have an existing app routing file, you have to edit adding the routers for login and the guard service. You also have to add the providers for the interceptor in the app module.

**If Custom Login option was selected:**

- authentication
  - login
    - login.component.ts
    - login.component.scss
    - login.component.spec.ts
    - login.component.ts
  - auth-user.interface.ts
  - auth.module.ts
  - auth.service.ts

If the flag --routing was set, the AppRouting file will be updated with the appRouting import.
