
![logo](_media/icon.png)

# Angular Schematics <small>0.0.46</small>

> A package to generate standard and reusable code in angular projects.

- Folder Structure
- Authentication modules
- 404 pages
- Spinners and loaders
- Guards

[GitLab](https://gitlab.prodigious.com/Prodigious/angular-schematics)
[Schematics](schematics/folder_structure)

<!-- background color -->

![color](#904e95)